var fs = require("fs");
var database = {
	file: './database.json',

	users: {},

	/*Methos used to initialize database*/
	init: function() {

		/*Get all data from database*/
		db = fs.readFileSync(database['file']).toString();

		/*Convert returned string to JSON*/
		db = JSON.parse(db);

		/*Initialize database's data*/
		database.users = db["users"];
	},

	/*Method used to prepare data for database save*/
	db: function() {
		var data = {
			"users": database.users
		};
		data = JSON.stringify(data, null, 4);
		return data;
	},

	/*Method used to save data to database*/
	saveData: function(callback) {
		fs.writeFile(database.file, database.db(), function (err) {
			if (err) 
			{
				console.log(err);
				return callback(err);
			}
			else
			{
				database.init();
				callback("ok");
			}
		});
	},

	/*Method used to register user*/
	register: function(user, callback) {
		database.users[user.username] = {
			password: user.password,
			isLogged: user.password == "facebook" ? false : true
		};

		if(user.password == "facebook")
			database.users[user.username]['isLogged'] = true;


		database.saveData(callback);
	},

	/*Method used to login user*/
	login: function(user, callback) {
		// database.users[user.username]['isLogged'] = true;
		callback("ok");
	},

	/*Method used to register children*/
	registerChild: function(child, callback) {
		database.users[child.username].children[child.name] = {
			lat: child.lat,
			lng: child.lng,
			perimetru: child.perimetru
		};
		database.saveData(callback);
	}

};
module.exports = database;