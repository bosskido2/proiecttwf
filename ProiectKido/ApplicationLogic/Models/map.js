Number.prototype.toRad = function() {
   return this * Math.PI / 180;
}

Number.prototype.toDeg = function() {
   return this * 180 / Math.PI;
}

google.maps.LatLng.prototype.destinationPoint = function(brng, dist) {
   dist = dist / 6371;  
   brng = brng.toRad();  

   var lat1 = this.lat().toRad(), lon1 = this.lng().toRad();

   var lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + 
                        Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

   var lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist) *
                                Math.cos(lat1), 
                                Math.cos(dist) - Math.sin(lat1) *
                                Math.sin(lat2));

   if (isNaN(lat2) || isNaN(lon2)) return null;

   return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
}
window.maps = {};
window.markers = {};

window.renderMap = function(map_id, data) {

   var pointA = new google.maps.LatLng(data.original.coordinates.lat, data.original.coordinates.lng);   // Circle center
   var radius = data.original.perimeter                                      // 10km

   var mapOpt = { 
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      center: pointA,
      zoom: 11
   };

   maps[map_id] = new google.maps.Map(document.getElementById(map_id), mapOpt);

   // Draw the circle
   if(data.moved_distance < data.original.perimeter )
   {
      new google.maps.Circle({
         center: pointA,
         radius: radius * 1000,       // Convert to meters
         fillColor: '#00FF00',
         fillOpacity: 0.2,
         map: maps[map_id]
      });
   }
   else
   {
      new google.maps.Circle({
         center: pointA,
         radius: radius * 1000,       // Convert to meters
         fillColor: '#FF0000',
         fillOpacity: 0.2,
         map: maps[map_id]
      });
   }

   // Show marker at circle center
   new google.maps.Marker({
      position: pointA,
      map: maps[map_id]
   });


   // Show marker at destination point
   markers[map_id] = new google.maps.Marker({
      position: pointA.destinationPoint(90, data.moved_distance),
      map: maps[map_id],
      title: data.child_name,
      icon: "http://localhost:8000/baby"
   });

   maps[map_id].setCenter(pointA.destinationPoint(90, data.moved_distance));


};


   renderMap("map", {
      original: {
         coordinates: {
            lat: 40.70,
            lng: -74.00
         },
         perimeter: 10
      },
      moved_distance: 8,
      child_name: 'test'
   });