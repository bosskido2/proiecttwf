
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

/*Set port*/
var port = 8000;

/*Import database functions*/
database = require('./database');

var activeKids = {};
var activeSockets = {};

var signal = "";

database.init();


/*Used to allow ajax calls*/
	app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "X-Requested-With");
	  next();
	});

/* Used to get api ajax calls data */
	var bodyParser = require('body-parser');
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	app.use(express.static(path.resolve(__dirname + "./")));
	app.use(express.static(path.resolve(__dirname + "./../")));
	app.use(express.static(path.resolve(__dirname + "./../../")));
	app.use(express.static(path.resolve(__dirname + "./../Models")));
	app.use(express.static(path.resolve(__dirname + "./../Views")));
	app.use(express.static(path.resolve(__dirname + "./../Controllers")));
	app.use(express.static(path.resolve(__dirname + "./../../Assets")));
	app.use(express.static(path.resolve(__dirname + "./../../Assets/Stylesheets")));
	app.use(express.static(path.resolve(__dirname + "./../../Assets/Images")));
	app.use(express.static(path.resolve(__dirname + "./../../Assets/Js")));
	app.use(express.static(path.resolve(__dirname + "./../../Assets/Js")));
	



/*Routes*/
	
	// Default response is a success response
	var defaultResponse = {
		code: 200,
		message: '',
		data: {}
	};

	app.get('/login', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Views/login.html"));
	});

	app.get('/baby', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../../Assets/Images/smallB.png"));
	});

	app.get('/', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../../index.html"));
	});

	app.get('/ApplicationLogic/Models/:file', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Models/" + req.params.file));
	});

	app.get('/ApplicationLogic/Controllers/:file', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Controllers/" + req.params.file));
	});
	app.get('/AplicationLogic/Controllers/:file', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Controllers/" + req.params.file));
	});

	app.get('/ApplicationLogic/Views/:file', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Views/" + req.params.file));
	});

	app.get('/Assets/:place/:file', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../../Assets/" + req.params.place + "/" + req.params.file));
	});
	app.get('/copii', function(req, res){
		res.sendFile(path.resolve(__dirname + "./../Views/copii.html"));
	});

	// Register user
	app.post('/signup', function(req, res){
		var user = req.body;

		var response = defaultResponse;

		/* if we find the user in our database */
		if(database.users[user.username])
		{   response.code=400;
			response.message = "Numele de utilizator exista deja!";
			
			res.json(response);	
		}
		else
			if(user.username==user.password)
			{response.code=400;
			response.message="Parola trebuie să fie diferită față de numele de utilizator!";
			res.json(response);
			}
			else 
		{
			/*we didnt find the user so we register him*/
			database.register(user, function(result){
				/*when we finish the registration, return response to user */

				if(result != "ok")
	  			{
	  				response.code = 400;
	  				response.message = result;
	  			}

	  			/*Send response to user*/
				res.json(response);	
			});
		}


	});

	// Login
	app.post('/login', function(req, res) {
		var user = req.body;
		var response = defaultResponse;


		/*If user is logged in*/
			if(database.users[user.username])
			{

				if(database.users[user.username].password == "facebook")
				{
					res.json(response);	
				}
				else
				{

					if(database.users[user.username].password == user.password)
					{

						var response = defaultResponse;
						return database.login(user, function(result) {
							if(result != "ok")
							{
								response.code = 400;
			  					response.message = result;
							}
							else
							{
								response = {
									code: 200,
									message: '',
									data: {}
								}
							}
							console.log(response);

							res.json(response);
						})
					}
					else
					{
						response.code = 400;
						response.message = "Verifică parola!!";
						res.json(response);
					}

				}
			}
			else
			{
				response.code = 400;
				response.message = "Numele de utilizator nu există!";
				res.json(response);
			}
		
	});


	// Add children
	app.post('/add-child', function(req, res) {
		var child = req.body;
		var response = defaultResponse;
		

		if(database.users[child.username])
		{
			/*If user doesnt have children property, we add it to him*/
			if(!database.users[child.username].children)
				database.users[child.username].children = {};

			/*If user does not have the specific child name in his children list, we save it*/
			if(!database.users[child.username].children[child.name])
			{
				database.registerChild(child, function(result){
					if(result != "ok")
					{
						response.code = 400;
	  					response.message = result;
					}
					res.json(response);
				});
			}
			else
			{
				res.json(response);
			}
		}	
		else
		{
			response.code = 400;
			response.message = "Trebuie sa te înregistrezi înainte de a adăuga un copil!!";
			res.json(response);
		}
	});

	// Get children for specific user
	app.post('/getChildren', function(req, res) {
		var user = req.body;
		var response = defaultResponse;
		response = {
					code: 200,
					message: '',
					data: {}
				}

		if(database.users[user.username])
		{
			if(!database.users[user.username].children || !database.users[user.username].children.length)
			{
				response.data = database.users[user.username].children; 				
				res.json(response);
			}
			else
			{
				res.json(response);
			}
		}	
		else
		{
			response.code = 400;
			response.message = "Nu se poate găsi contul cu acest nume de utilizator! " + user.username + " = ?";
			console.log(database.users);
			res.json(response);
		}

		if(response.code == 200)
		{
			setInterval(function(){
				for (kid in activeKids)
				{
					console.log(kid);
					console.log(activeKids[kid].moved_distance);
					/*Increase distance*/
					activeKids[kid].moved_distance = activeKids[kid].moved_distance+1;

					io.emit('move', {
						child: kid,
						moved_distance: activeKids[kid].moved_distance,
						map: activeKids[kid]
					});
				}
			},3000);

		}
	});

	// Delete children
	app.post('/delete-child', function(req, res) {
		var child = req.body;
		var response = defaultResponse;
		

		if(database.users[child.username])
		{
			/*If user doesnt have children property, we add it to him*/
			if(!database.users[child.username].children)
				alert("Nu aveți copii înregistrați!");

			
			if(database.users[child.username].children[child.name])
			{
				database.removeChild(child, function(result){
					if(result != "ok")
					{
						response.code = 400;
	  					response.message = result;
					}
					else
					{
						response = {
								code: 200,
								message: '',
								data: {}
							}
					}
					res.json(response);
				});
			}
			else
			{
				res.json(response);

			}
		}	
		else
		{
			response.code = 400;
			response.message = "Trebuie sa te înregistrezi înainte de a adăuga un copil!";
			res.json(response);
		}
	});



io.sockets.on('connection', function(socket){

	socket.on('reg', function(data){

		activeSockets[data.child] = socket.id;
		activeKids[data.child] = {
			original: {
		      coordinates: {
		         lat: database.users[data.username].children[data.child].lat,
		         lng: database.users[data.username].children[data.child].lng
		      },
		      perimeter: database.users[data.username].children[data.child].perimetru
		   },
		   moved_distance: 0,
		   child_name: data.child
		};

	});
});


io.sockets.on('disconnected', function(socket){
	console.log(socket.id + " disconnected");
	activeSockets.pop(socket.id);
});





/*Start server*/
http.listen(port, function(){
  console.log('listening on port',port);
});
