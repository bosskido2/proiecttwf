/*When document is ready*/
var baseUrl = "http://localhost:8000";

var user = "";

var bara=document.getElementById('div4');


/*------------------????????????--------------------------*/
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
/*------------------????????????--------------------------*/


/*------------!!!!!!!!!!!!!!!!!-----------*/
function goToByScroll(id){
          // Reove "link" from the ID
        id = id.replace("link", "");
          // Scroll
        $('html,body').animate({
            scrollTop: $("#"+id).offset().top},
            'slow');
 }

function loadTot()
{
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/meniu.html", success: function(result){
            $("#meniu").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/home.html", success: function(result){
            $("#home").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/servicii.html", success: function(result){
            $("#servicii-id").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/despre.html", success: function(result){
            $("#despre").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/echipa.html", success: function(result){
            $("#echipa").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/login.html", success: function(result){
            $("#login-tot").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/signup.html", success: function(result){
            $("#signup-id").html(result);
        }});
    });
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/multumire.html", success: function(result){
            $("#multumire").html(result);
        }});
    });
}

function apareSignup(){
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/signup.html", success: function(result){
            $("#signup-id").html(result);
        }});
        goToByScroll('signup-id');
    });
}

function apareLogin(){
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/login.html", success: function(result){
            $("#login-to").html(result);
        }});
        goToByScroll('login-tot');
    });
}

function goLoginShowSignup(){
	$(document).ready(function(){
        goToByScroll('login-tot');
        $.ajax({url: "ApplicationLogic/Views/signup.html", success: function(result){
            $("#signup-id").html(result);
        }});
    });
}


function loadDelogat(){
    $(document).ready(function(){
	$.ajax({url: "ApplicationLogic/Views/login.html", success: function(result){
            $("#login-tot").html(result);
        }});
	$.ajax({url: "ApplicationLogic/Views/meniu.html", success: function(result){
            $("#meniu").html(result);
        }});
	   history.go(-1);
    });
}


/*---------!!!!!!!!!!!!!!!!------------*/

var redirectTo = function(page){
		if(page != undefined)
			window.location.href = './' + page;
	};

var apiSuccess = function(data){
		return data.code == 200;
	};

var logFailure = function(data){
		if(!localStorage["user"] || localStorage["user"] == "")
		{
			if(data && data.message != undefined)
				alert(data.message);
		}
		else
		{
			if(data.message.indexOf("your password again") == -1)
				alert(data.message);

		}
	};

var loadContCreat=function(){
	$(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/creat.html", success: function(result){
            $("#signup-id").html(result);
        }});
       	goToByScroll('signup-id');
    });
};


var loadLogat=function(){
    $(document).ready(function(){
       // $.ajax({url: "ApplicationLogic/Views/copii.html", success: function(result){
          //  $("#login-tot").html(result);

           /* setTimeout(function() {
				if(typeof localStorage['user'] == "undefined")
				{
	            	redirectTo("./../Views/copii.html")
				}
            	// redirectTo("copii");
            },2000);
        //}});*/
        $.ajax({url: "ApplicationLogic/Views/meniu_logat.html", success: function(result){
            $("#meniu").html(result);
        }});
        redirectTo("copii")
   });
};

var loadChildren = function() {
	socket =  io('http://localhost:8000');


	console.log(localStorage);

	var data = {};
	data.username = localStorage['user'];
	$.ajax({
		type: "POST",
		url: baseUrl + "/getChildren",
		data: data
	})
	.done(function( response ) {
		/*If everything is ok*/
		if(apiSuccess(response))
		{
			var listaCopii = "";
			var verificare="";

			for (var child in response.data){
				var childId = child;
				childId = childId.replace(" ", "");

				listaCopii += "<li id='"+ childId +"id'>" + "<span class='nume-copil'>"+child + "</span><ul>";

				/*Show distance, real time*/
				listaCopii += "<li class='dist'>0m</li>";
				listaCopii += "<li class='status'>Situatie: Copilul sta pe loc</li>";

				listaCopii += "<li class='no-map'><button class='showMap home-buton' data-id='"+childId+"'>Show Map</button></li>";
				listaCopii += "</ul> </li>";



				socket.childId = childId;

				socket.emit("reg", {
					username: data.username,
					child: child
				});

			}
			if(listaCopii==""){
				verificare+="Nu aveti copii înregistrati încă!";
				$("#copilNume").hide();
				$("#delete-child").hide();
			}
			else{
				verificare+="Copiii pe care i-ai înregistrat sunt:";
			}
			$(".lista-copii").html(listaCopii);
			$(".copii-verificare").html(verificare);


			$(".showMap").click(function(){
				$("#map").show();
				var childId = $(this).attr("data-id");
				var numeCopilHarta="Harta pentru copilul "+"<span class='nume-copil-italic'>"+childId+"</span> este:";
				$(".nume-copil-harta").html(numeCopilHarta);
				$(".nume-copil-harta").show();
				$("#map").attr('class', childId);
				$("#copilNume").show();
				$("#delete-child").show();
			});

			socket.on('move', function (data) {
				var id = data.child.replace(" ", "");

				/*Update the distance*/
				$("#" + id + "id").find(".dist").text("Distanta parcursa: " + data.moved_distance*10 + "m");

				if(data.moved_distance >= data.map.original.perimeter)
				{
					$("#" + id + "id").find(".status").html("Status: Pericol, copilul s-a îndepartat prea mult!");
				}
				else
				{
					$("#" + id + "id").find(".status").html("Status: Copilul este în siguranță, dar se îndepărtează de zonă")

				}

				if($("#map").hasClass(id))
				{
					renderMap("map", data.map);
				}
			});
		}
		else
		{
			/* Alert the user that we have a problem */
			logFailure(response);
		}
	});
};

var facebookSignup = function(response){
	var data = {};
	data.username = response.name;
	data.password = "facebook";

	$.ajax({
		type: "POST",
		url: baseUrl + "/signup",
		data: data
	})
	.done(function( response ) {
		/*If everything is ok*/
		if(apiSuccess(response))
		{
			user = response.name;
			/* Redirect to main page */
			loadLogat();
		}
		else
		{
			/* Alert the user that we have a problem */
        	//window.location.href="/copii?user=" + response.name;

			redirectTo('copii');
		}
	});
}

$(".showMap").click(function(){
	$("#map").show();
	var childId = $(this).attr("data-id");
	$("#map").attr('class', childId);
});
$(document).ready(function() {

	if(!$("#onChildrenPage").length)
	{
		if(typeof localStorage['user'] != "undefined" || user != "" )
		{
			/*It means one user is logged in so we send him the children and status for each one*/
				//redirectTo('copii');
				$(".copii-mesaj").text("Bine ai venit, " + localStorage['user'] + "!");
				loadLogat();
			}
	}
	else
	{
		$(".copii-mesaj").text("Bine ai venit, " + localStorage['user'] + "!");
		loadChildren();
	}

    $(window).scroll(function () {
      	console.log($(window).scrollTop())
    	if ($(window).scrollTop() > 150) {
      		$('#meniu').addClass('meniu-in-jos').addClass('meniu-variante-in-jos');
      		$('#meniu').addClass('meniu-variante-in-jos');
    	}
    	if ($(window).scrollTop() < 151) {
      		$('#meniu').removeClass('meniu-in-jos');
      		$('#meniu').removeClass('meniu-variante-in-jos');
    	}
  });


  	$(".signup-submit").click(function(e){
		/*Prevent form submission*/
		e.preventDefault();
		var data = {};

		/*This flag will be used for validation*/
		var canProceed = true;

		/*Get username and passwords and prepare for ajax request*/
		data.username = $("#username-signup").val();
		data.password = $("#password-signup").val();
		data.repassword = $("#re-password-signup").val();

		if(data.username == "" || data.password == "" || data.repassword == "")
		{
			alert("Introduceti toate datele!")
			canProceed = false;
		}else
		/*Validate passwords*/
		if(data.password != data.repassword)
		{
			alert("Parolele nu se potrivesc!");
			canProceed = false;
		}else{
			if(data.username.length<8){
				alert("Username-ul trebuie sa aiba minim 8 caratere!");
				canProceed = false;
			}else
			{
				if(data.password.length<8){
					alert("Parola trebuie sa aiba minim 8 caratere!");
					canProceed = false;
				}
			}
		}
		/*If our flag is true, we can proceed to the ajax call*/
		if(canProceed)
		{	
			$.ajax({
				type: "POST",
				url: baseUrl + "/signup",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					loadContCreat();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(response);
				}
			});
		}
	});



	$(".login-submit").click(function(e){
		/*Prevent form submission*/
		e.preventDefault();
		
		var data = {};

		/*This flag will be used for validation*/
		var canProceed = true;

		/*Get username and passwords and prepare for ajax request*/
		data.username = $("#username-login").val();
		data.password = $("#password-login").val();

		if(data.password == "" || data.username == "")
		{
			alert("Introduceti si username-ul si parola!");
			canProceed = false;
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/login",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				var res = response;
				if(apiSuccess(res))
				{
					/* Redirect to main page */
					localStorage["user"] = data.username;
					loadLogat();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(res);
				}
			});
		}
	});


	/*$("#logout").click(function(){
		localStorage.removeItem('user');
		loadDelogat();
	});*/

	$("#logout-copii").click(function(){
		localStorage.removeItem('user');
		loadDelogat();
		//redirectTo('../../index.html');
	});


	$(".addChild").click(function(e) {
		e.preventDefault();

		$(".cont.lista").hide();
		$(".cont.adauga-copil").show();
		$("#copilNume").hide();
		$("#delete-child").hide();
	});

	$("#cancel-add").click(function(e) {
		e.preventDefault();

		$(".cont.lista").show();
		$(".cont.adauga-copil").hide();
		$("#copilNume").show();
		$("#delete-child").show();
	});


	$("#add-child").click(function(e){
		e.preventDefault();

		var data = {};
		var canProceed = true;

		data.name = $("#nume").val();
		data.lat = $("#lat").val();
		data.lng = $("#lng").val();
		data.perimetru = $("#perimetru").val();
		data.username = localStorage['user'];

		if(data.name == "" || data.lat == "" || data.lng == "" || data.perimetru == "")
		{
			alert("Completati toate campurile!");
			canProceed = false;	
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/add-child",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					$(".cont.lista").show();
					$(".cont.adauga-copil").hide();
					$(".nume-copil-harta").hide();
					$("#copilNume").show();
					$("#delete-child").show();

					loadChildren();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(response);
					//alert("Exista deja un copil cu acest nume!");
				}
			});
		}

	});



	$("#delete-child").click(function(e){

		e.preventDefault();

		var data = {};
		var canProceed = true;

		data.name = $("#copilNume").val();
		data.username = localStorage['user'];

		if(data.name == "")
		{
			alert("Introduceti numele copilului!");
			canProceed = false;	
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/delete-child",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					$(".cont.lista").show();
					$(".cont.adauga-copil").hide();
					$(".nume-copil-harta").hide();
					$("#copilNume").show();
				$("#delete-child").show();

					loadChildren();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(response);
				}
			});
		}


	});


});